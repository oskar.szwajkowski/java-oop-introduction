## Zadanie z obiektowości - Java

####background:

Musisz wykonać projekt obsługi fabryki samochodów.
Projekt powinien spełniać kilka wymagań, podanych nizej. Należy zacząć od implementacji modeli obiektów, które aplikacja będzie wykorzystywać. 

Kolejnym krokiem będzie napisanie logiki, tak aby zestaw przygotowanych testów przechodził.

Potem zostanie czas na refactoring, który jest tak samo ważny jak to aby kod działał.

*** 
#### zadanie 1:
> Każdą z klas stwórz w osobnym pliku
1. Stwórz enum ```BRAND```, posiadający następujące wartości:
    * AUDI
    * SKODA
    * SEAT
1. Stwórz enum ``FUEL_TYPE``, posiadający następujące wartości:
    * DIESEL
    * PETROL
    * HYBRID

1. Stwórz klasę ``InvalidColorException`` rozszerzającą klasę ``RuntimeException``, posiadający konstruktor:
    * konstuktor przyjmujący parametry:
        * ``red``, typu int
        * ``green``, typu int
        * ``blue``, typu int
        
        Konstruktor powinien wywołać konstruktor klasy bazowej (``super``), przekazując w parametrach wiadomość:
        > Invalid color specified, color RED GREEN BLUE is not valid RGB color
        
        gdzie za RED, GREEN, BLUE powinny zostać podstawione wartości kolorów przekazane w parametrach

1. Stwórz klasę ```Color```, posiadająca następujące pola:
    * pole ```red```, które jest liczbą całkowitą (```int```)
    * pole ```green```, które jest liczba całkowita (```int```)
    * pole ```blue```, które jest liczbą całkowitą (```int```)
    * pole ```name```, które jest ciągiem znaków (```String```)
    * konstruktor przyjmujący parametry:
        * ``red``, typu ``int``
        * ``green``, typu ``int``
        * ``blue``, typu ``int``
        * ``name``, typu ``String``
        
        Konstruktor ten powinien sprawdzać czy wszystkie wartości ``red``, ``green``, ``blue`` są w przedziale między 0 (włącznie), a 255 (włącznie), jeżeli nie, powinien zostać rzucony (``throws``) wyjątek ``InvalidColorException``

1. Stwórz klasę ```Car```, posiadającą następujące pola:
    * pole ```brand```, które jest typu (```BRAND```)
    * pole ```color```, które jest typu ```Color```
    * pole ```model```, które jest ciągiem znaków (```String```)
    * pole `productionDate`, które jest typu ```LocalDate```
    * pole `fuelType`, które jest typu ``FUEL_TYPE``

1. Stwórz interfejs (```interface```) ``CarFactory`` , posiadający następujące metody:
    * metoda createCar, zwracająca ```Car```, przyjmująca parametry:
        * color, typu ``Color``
        * model, typu ``String``
        * brand, typu ``BRAND``
    * metoda repaintCar, zwracająca ``Car``, przyjmująca parametry:
        * car, typu ``Car``
        * targetColor, typu ``Color``

1. Stwórz klasę ``RepaintException``, rozszerzającą ``extends`` klasę ``RuntimeException``

1. Zrób commit i push swojego brancha

1. Stwórz klasę ``BasicCarFactory`` implementującą interfejs ``CarFactory``
    * metoda ``createCar``, powinna zwracać samochód uzupełniony danymi przekazanymi w parametrach, dodatkowo z uzupełnioną dzisiejszą datą produkcji (`productionDate`)
    
    * metoda ``repaintCar``, powinna zwracać nowy obiekt typu ``Car``, ze zmienionym kolorem, zgodnie z parametrem, sprawdzając uprzednio kilka warunków:
        * wartości kolorów nie powinny być od siebie różne o więcej niż 120 każdy, przykład:
            - red - 255, green  - 240, blue - 220, JEST OK: 
                * |255 - 240| < 120
                * |255 - 220| < 120
                * |240 - 120| < 120
            - red - 255, green - 110, blue - 230 JEST NIE OK:
                * **|255 - 110| > 120**
                * |255 - 230| < 120
                * **|110 - 230| == 120**
        
        Jeżeli warunek jest spełniony, powinien zostać zwrócony nowy obiekt, w przeciwnym przypadku powinien zostać rzucony (`throws`)  wyjątek `RepaintException`  
